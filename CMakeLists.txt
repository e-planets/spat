cmake_minimum_required(VERSION 2.6)
set(CMAKE_MODULE_PATH ${CMAKE_CURRENT_SOURCE_DIR}/cmake_modules)
include(FindPythonModule)
include(UsePython)

project(spat NONE)

option(BUILD_TESTING "Built the test programs" False)

find_package(PythonInterp REQUIRED)
configure_file(
    cmake_modules/configure_python_script.cmake.in
    ${CMAKE_BINARY_DIR}/configure_python_script.cmake
    @ONLY)
set(PYTHON_SITELIBDIR
    lib/python${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}/site-packages)
find_python_module(numpy REQUIRED)
find_python_module(scipy REQUIRED)
find_python_module(osgeo.gdal REQUIRED)
find_python_module(matplotlib REQUIRED)

add_subdirectory(spat)

install(
    FILES convert_uomega_files_to_envi.py plot_spectrum.py make_crism_summary_products.py  plot_spectrum2d.py
    DESTINATION bin
    PERMISSIONS OWNER_READ OWNER_WRITE OWNER_EXECUTE GROUP_READ GROUP_EXECUTE WORLD_READ WORLD_EXECUTE)

if(BUILD_TESTING)
  include(CTest)
  add_subdirectory(tests)
endif(BUILD_TESTING)

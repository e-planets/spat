# How to contribute to spat

If you want to help, here are a few guidelines to help us and you :)

## Do not work from conda or any packaged release

Retrieve the project source code from this repository:

```
$ git clone git@forge.univ-lyon1.fr:e-planets/spat.git
$ cd spat
```

## Create a branch

If you have a specific idea, create a branch with that name in your local
repository. You can also have a branch named after yourself if you want
to isolate yourself longterm from the master -- but those are unlikely to
be merged back. For example, if you want to add a new option foo to 
`plot_spectrum`:

```
$ git checkout -b plot_spectrum_foo
```

Change things as needed. You are encouraged when reaching interesting
milestones in your work to commit those steps. You can also send the branch
to the main respository for safekeeping or feedback seeking:

```
$ git push -u origin plot_spectrum_foo
```

## Run the test suite

spat has a test suite to try to catch errors. Before submitting your work,
and probably during your development, you should run it and be sure it runs
fine:

```
$ cd spat
$ mkdir build
$ cd build
$ cmake -DBUILD_TESTING=yes ..
$ make
$ make test
```

## Submit your work

Notify the maintainers or create a pull request.

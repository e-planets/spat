set(CTEST_PROJECT_NAME "spat")
set(CTEST_NIGHTLY_START_TIME "01:00:00 UTC")

set(CTEST_DROP_METHOD "http")
set(CTEST_DROP_SITE "geos-mercury.univ-lyon1.fr")
set(CTEST_DROP_LOCATION "/cdash/submit.php?project=spat")
set(CTEST_DROP_SITE_CDASH TRUE)

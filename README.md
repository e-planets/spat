# Spectrum Plotting and Analysis Tools

A collection of tools to work on spectrometry results. spat is developed for
the purposes of the [e-Planets](http://eplanets.univ-lyon1.fr/) research
workgroup.

spat source code is free/opensource and is available under the terms of the 
CeCILL-B that you can find at the root of the source code.


## Requirements

spat will require some software to work:
 * Python (>= 2.6, >= 3.4 recommended)
  * Matplotlib (>=3.0)
  * Numpy (>= 1.11)
  * Scipy (>= 0.18)
 * GDAL (>=2.0, with python bindings)
  

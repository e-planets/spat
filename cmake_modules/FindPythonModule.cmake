function(find_python_module module)
  string(TOUPPER ${module} module_upper)
  if(NOT PY_${module_upper})
    if(ARGC GREATER 1 AND ARGV1 STREQUAL "REQUIRED")
      set(PY_${module}_FIND_REQUIRED TRUE)
    endif()
    # A module's location is usually a directory, but for binary modules
    # it's a .so file.
    if(${PYTHON_VERSION_MAJOR} GREATER 2)
      set(PYTHON_TEST_IMPORT "import re, ${module}; print(re.compile('/__init__.py.*').sub('',${module}.__file__))")
    else(${PYTHON_VERSION_MAJOR} GREATER 2)
      set(PYTHON_TEST_IMPORT "import re, ${module}; print re.compile('/__init__.py.*').sub('',${module}.__file__)")
    endif(${PYTHON_VERSION_MAJOR} GREATER 2)
    execute_process(COMMAND "${PYTHON_EXECUTABLE}" "-c" "${PYTHON_TEST_IMPORT}"
      RESULT_VARIABLE _${module}_status 
      OUTPUT_VARIABLE _${module}_location
      ERROR_QUIET OUTPUT_STRIP_TRAILING_WHITESPACE)
    if(NOT _${module}_status)
      set(PY_${module_upper} ${_${module}_location} CACHE STRING 
        "Location of Python module ${module}")
    endif(NOT _${module}_status)
  endif(NOT PY_${module_upper})
  find_package_handle_standard_args(PY_${module} DEFAULT_MSG PY_${module_upper})
endfunction(find_python_module)

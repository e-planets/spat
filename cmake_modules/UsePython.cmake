macro(ADD_PYTHON_SCRIPT target source)
  if(EXECUTABLE_OUTPUT_PATH)
    set(_dir ${EXECUTABLE_OUTPUT_PATH})
  else()
    set(_dir ${CMAKE_CURRENT_BINARY_DIR})
  endif()
  add_custom_command(
      OUTPUT ${EXECUTABLE_OUTPUT_PATH}/${target}
      COMMAND ${CMAKE_COMMAND} -DINPUT=${CMAKE_CURRENT_SOURCE_DIR}/${source} -DOUTPUT=${_dir}/${target} -P ${CMAKE_BINARY_DIR}/configure_python_script.cmake
      COMMAND ${PYTHON_EXECUTABLE} -m py_compile ${EXECUTABLE_OUTPUT_PATH}/${target}
      COMMAND chmod 755 ${_dir}/${target}
      DEPENDS ${source}
      COMMENT "Configuring Python script ${target}"
      VERBATIM)
  add_custom_target(configure_${target} ALL DEPENDS ${EXECUTABLE_OUTPUT_PATH}/${target})
endmacro()

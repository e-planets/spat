@ECHO OFF

MKDIR build
CD build
cmake -GNinja -DCMAKE_INSTALL_PREFIX:PATH=%PREFIX% -DPYTHON_EXECUTABLE:PATH=%PYTHON% ..
cmake --build .
cmake -P cmake_install.cmake

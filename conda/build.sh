#!/bin/sh
set -e

mkdir build
cd build
cmake -G'Unix Makefiles' \
	-DCMAKE_INSTALL_PREFIX:PATH=${PREFIX} \
	-DPYTHON_EXECUTABLE:PATH=$PYTHON \
	..
cmake --build .
cmake -P cmake_install.cmake

#!/usr/bin/env python3

import sys

import numpy as np
from osgeo import gdal

from spat import uomegautils

cube, wvls = uomegautils.loadcube(sys.argv[1])
envidrv = gdal.GetDriverByName('ENVI')
out_ds = envidrv.Create(sys.argv[2],
                        cube.shape[1], cube.shape[0], cube.shape[2],
                        gdal.GDT_Float32)
for z in range(out_ds.RasterCount):
    out_band = out_ds.GetRasterBand(z+1)
    out_band.WriteArray(cube[:, :, z])
    out_band.SetMetadata({'wavelength': str(wvls[z])})
out_md = out_ds.GetMetadata('ENVI')
out_md['wavelength'] = '{'+','.join([str(w) for w in wvls])+'}'
out_md['wavelength_units'] = 'Micrometers'
out_ds.SetMetadata(out_md, 'ENVI')
del out_ds

#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import getopt
import os
import sys

import numpy as np
from osgeo import gdal
from osgeo import osr

from spat.cube import getwvl
import crismutils

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

if sys.version_info[0] < 3:
    import Tkinter as tk
    import tkFileDialog as tkfiledialog
else:
    import tkinter as tk
    import tkinter.filedialog as tkfiledialog


# Load the geographic lookup table associated with a CRISM CAT cube
def loadglt(path, xcrop=0, ycrop=28):
    ds = gdal.Open(path)
    if not ds:
        exit(1)
    xsize, ysize, zsize = ds.RasterXSize, ds.RasterYSize, ds.RasterCount
    de_name = os.path.basename(path)
    de_path = os.path.join(os.path.dirname(path),
                           de_name[0:14]+"_DE"+de_name[17:21]+"_DDR1.img")
    de_data = np.fromfile(de_path, dtype=np.float32).reshape((14, ysize, xsize))
    lats = de_data[3, :ysize-ycrop, :]
    lons = de_data[4, :ysize-ycrop, :]
    return lons, lats


# Collection of functions returning summary products from Viviano-Beck & al.
def r770(cube, wvls):
    return getwvl(cube, wvls, 770)

def rbr(cube, wvls):
    return r770(cube, wvls) / getwvl(cube, wvls, 440)

def rir(cube, wvls): # From Lu
    return getwvl(cube, wvls, 984) / r770(cube, wvls)

def rslope(cube, wvls): # From Lu
    return getwvl(cube, wvls, 984) / getwvl(cube, wvls, 440)

def bd530_2(cube, wvls): # From Viviano et Al.
    a = (530-440) / (614-440)
    b = 1 - a
    return 1 - (getwvl(cube, wvls, 530) / (b*getwvl(cube, wvls, 440) + a*getwvl(cube, wvls, 614)))


# Reproject a layer according to a geographic lookup table
def gltreproj_nearest(band, arr, lons, lats):
    xsize, ysize = band.XSize, band.YSize
    lonmin, lonmax = lons.min(), lons.max()
    latmin, latmax = lats.min(), lats.max()
    projarr = np.full((ysize, xsize), np.nan)
    for y in range(lons.shape[0]):
        for x in range(lons.shape[1]):
            newx = int((lons[y, x] - lonmin) / (lonmax - lonmin) * (xsize-1))
            newy = ysize-1-int((lats[y, x] - latmin) / (latmax - latmin) * (ysize-1))
            projarr[newy, newx] = arr[y, x]
    band.WriteArray(projarr)
    gdal.FillNodata(band, None, 4.0, 2)
    del projarr


if __name__ == "__main__":
    # Constants
    xres, yres = 19.1, 19.1
    deg2m = 59274.69752330622

    # Load the gtiff driver
    drv = gdal.GetDriverByName("gtiff")

    # Retrieve path from arguments or pop a dialog window for those
    # poor spyder users :)
    paths = None
    if len(sys.argv) > 1:
        paths = sys.argv[1:]
    else:
        # Create TK window, but withdraw it immediately in case we don't
        # plot anything
        root = tk.Tk()
        root.withdraw()
        paths = root.tk.splitlist(tkfiledialog.askopenfilenames())

    for p in paths:
        cube, wvls = crismutils.loadcube(p)
        if p.startswith("FRT"):
            lons, lats = loadglt(p, xcrop=0, ycrop=0)
        else:
            lons, lats = loadglt(p, xcrop=0, ycrop=28)
        lonorigin = np.median(lons)
        latorigin = (int((int(lats.max())+4)/5)-1)*5

        # Create an output dataset
        out_xsize = int((lons.max() - lons.min()) / (xres/deg2m))
        out_ysize = int((lats.max() - lats.min()) / (yres/deg2m))
        out_ds = drv.Create(os.path.splitext(p)[0]+"_PARAM.tiff",
                            out_xsize, out_ysize, 5,
                            gdal.GDT_Float32)
        out_ds.GetRasterBand(1).SetNoDataValue(np.nan)

        # Create the geotransformation matrix
        out_gt = (
            (lons.min()-lonorigin)*deg2m,
            xres,
            0.0,
            (lats.max())*deg2m,
            0.0,
            -yres)
        # And set the projection
        out_ds.SetGeoTransform(out_gt)
        out_srs = osr.SpatialReference()
        out_srs.ImportFromProj4("+proj=eqc +lat_ts=%d +lon_0=%.8f +lat_0=0 +x_0=0 +y_0=0 +a=3396190 +b=3396190 +units=m +no_defs" % (latorigin, lonorigin))
        out_ds.SetProjection(out_srs.ExportToWkt())

        # Write each band with appropriate data
        out_band_r770 = out_ds.GetRasterBand(1)
        gltreproj_nearest(out_band_r770, r770(cube, wvls), lons, lats)
        out_band_rbr = out_ds.GetRasterBand(2)
        gltreproj_nearest(out_band_rbr, rbr(cube, wvls), lons, lats)
        out_band_rir = out_ds.GetRasterBand(3)
        gltreproj_nearest(out_band_rir, rir(cube, wvls), lons, lats)
        out_band_rslope = out_ds.GetRasterBand(4)
        gltreproj_nearest(out_band_rslope, rslope(cube, wvls), lons, lats)
        out_band_bd5302 = out_ds.GetRasterBand(5)
        gltreproj_nearest(out_band_bd5302, bd530_2(cube, wvls), lons, lats)

        if True:
            plt.subplots_adjust(wspace=0.3)
            plt.suptitle(os.path.splitext(os.path.basename(p))[0]+"_PARAM.tiff")
            plt.subplot(231)
            plt.title("R770")
            plt.imshow(out_band_r770.ReadAsArray(), cmap="inferno")
            plt.subplot(232)
            plt.title("RBR")
            plt.imshow(out_band_rbr.ReadAsArray(), cmap="inferno")
            plt.subplot(233)
            plt.title("RIR")
            plt.imshow(out_band_rir.ReadAsArray(), cmap="inferno")
            plt.subplot(234)
            plt.title("RSlope")
            plt.imshow(out_band_rslope.ReadAsArray(), cmap="inferno")
            plt.subplot(235)
            plt.title("BD530/2")
            plt.imshow(out_band_bd5302.ReadAsArray(), cmap="inferno")
            plt.savefig(os.path.splitext(p)[0]+"_PARAM_QUICKLOOK.pdf", dpi=150)

        del out_ds # close dataset

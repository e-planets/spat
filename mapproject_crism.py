#!/usr/bin/env python3

# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import getopt
import os
import sys

import numpy as np
from scipy.ndimage import zoom
from osgeo import gdal

from spat.crismutils import loadglt, glt2plt

# Create a projection
def createproj(latlt):
    reflat = np.median(latlt)
    if np.absolute(reflat) <= 65.0: # equatorial
        proj = '''PROJCS["Mars Equirectangular Default",GEOGCS["GCS_Unknown",DATUM["D_Unknown",SPHEROID["S_Unknown",3396190,0]],PRIMEM["Greenwich",0],UNIT["Degree",0.0174532925199433]],PROJECTION["Equirectangular"],PARAMETER["standard_parallel_1",0],PARAMETER["central_meridian",0],PARAMETER["false_easting",0],PARAMETER["false_northing",0],UNIT["metre",1,AUTHORITY["EPSG","9001"]],AXIS["Easting",EAST],AXIS["Northing",NORTH]]'''
    else:
        raise Exception('Polar observation not supported yet')
    return proj

# Reproject a layer according to a geographic lookup table
def gltreproj_nearest_v0(projarr, arr, rxlt, rylt):
    for y in range(projarr.shape[0]):
        for x in range(projarr.shape[1]):
            if rxlt[y, x] < 0 or rylt[y, x] < 0:
                projarr[y, x] = np.nan
            else:
                origx, origy = rxlt[y, x], rylt[y, x]
                projarr[y, x] = arr[origy, origx]
def gltreproj_nearest(projarr, arr, rxlt, rylt):
    for y, x in np.argwhere(rxlt >= 0):
        origx, origy = rxlt[y, x], rylt[y, x]
        projarr[y, x] = arr[origy, origx]

if __name__ == "__main__":
    # Constants
    deg2m = 59274.69752330622

    # Load the driver
    drv = gdal.GetDriverByName("envi")

    # Arguments
    inpath, outpath = sys.argv[1], sys.argv[2]

    # Should we check cat bin mode rather than image filename?
    inpathname = os.path.basename(inpath)
    mode = inpathname[:3].upper()
    if mode in ['FRT', 'ATO', 'ATU']:
        xres = yres = 18.0
    elif mode in ['FRS']:
        xres = yres = 25.0
    elif mode in ['HRS', 'HRL']:
        xres = yres = 36.0
    elif mode in ['MSW', 'MSV', 'MSP', 'HSP', 'HSV']:
        xres = yres = 200.0
    else:
        raise Exception('Resolution unkown for mode '+mode)

    # Load input and associated geolookup table
    in_ds = gdal.Open(inpath)
    if in_ds == None:
        sys.exit(1)
    latlt, lonlt = loadglt(inpath)

    # Create an output dataset
    out_xsize = int((lonlt.max() - lonlt.min()) / (xres/deg2m))
    out_ysize = int((latlt.max() - latlt.min()) / (yres/deg2m))
    out_nbands = in_ds.RasterCount
    out_ds = drv.Create(outpath,
                        out_xsize, out_ysize, out_nbands,
                        gdal.GDT_Float32)
    # Nodata
    out_ds.GetRasterBand(1).SetNoDataValue(np.nan)
    # Create the geotransformation matrix
    out_gt = (
        lonlt.min()*deg2m,
        xres,
        0.0,
        latlt.max()*deg2m,
        0.0,
        -yres)
    out_ds.SetGeoTransform(out_gt)
    # And set the projection
    out_proj = createproj(latlt)
    out_ds.SetProjection(out_proj)
    # Metadata
    if 'ENVI' in in_ds.GetMetadataDomainList():
        # Is everything still once georef? (size is handled by gdal)
        out_ds.SetMetadata(in_ds.GetMetadata('ENVI'), 'ENVI')

    # Convert lookup tables from geo coords to pixel coords
    xlt, ylt = glt2plt(lonlt, latlt, out_xsize, out_ysize)
    # Oversample, so we can resolve every output pixel
    xlt = zoom(xlt, 4, order=3)
    ylt = zoom(ylt, 4, order=3)
    # Create reverse tables
    rxlt = np.full((out_ysize, out_xsize), -1, np.int32)
    rylt = np.full((out_ysize, out_xsize), -1, np.int32)
    for y in range(xlt.shape[0]):
        for x in range(xlt.shape[1]):
            newx, newy = int(round(xlt[y, x])), int(round(ylt[y, x]))
            rxlt[newy, newx] = int(x/4)
            rylt[newy, newx] = int(y/4)

    del lonlt, latlt, xlt, ylt

    arr = np.empty((in_ds.RasterYSize, in_ds.RasterXSize))
    projarr = np.empty((out_ysize, out_xsize))
    projarr[rxlt < 0] = np.nan

    # Write each band with appropriate data
    for z in range(out_nbands):
        in_b = in_ds.GetRasterBand(z+1)
        out_b = out_ds.GetRasterBand(z+1)

        in_b.ReadAsArray(buf_obj=arr)
        arr[arr == in_b.GetNoDataValue()] = np.nan
        gltreproj_nearest(projarr, arr, rxlt, rylt)
        out_b.WriteArray(projarr)

    del out_ds # close dataset

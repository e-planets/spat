#!/usr/bin/env python3

# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import sys

import numpy as np

import tkinter as tk
import tkinter.filedialog as tkfiledialog

import matplotlib
matplotlib.use('TkAgg')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

from spat.spectrum import rm_continuum, filter, resample, normalize


# Options
smoothwsize = 5

# Helper function to load the data
def load_specta(path):
    fname, fext = os.path.splitext(os.path.basename(path))
    try:
        wvl, val, err = np.loadtxt(path,
                                   skiprows=4,
                                   unpack=True,
                                   encoding='iso-8859-1')
    except TypeError:
        wvl, val, err = np.loadtxt(path,
                                   skiprows=4,
                                   unpack=True)
    return [(wvl, val, fname), (wvl, err, fname+'_err')]

if __name__ == "__main__":
    # Callback function when stuf is changed in the GUI
    def update_plot():
        global data, ax, canvas, do_rmcontinuum, do_smoothen, do_normalize, smoothwsize
        ax.clear()
        ax.grid(axis="x")
        for wvl, val, name in data:
            x, y = wvl, val
            if do_rmcontinuum.get():
                x, y = rm_continuum(x, y)
            if do_smoothen.get():
                y = filter(x, y, smoothwsize)
                x, y = resample(x, y)
            if do_normalize.get():
                y = normalize(x, y)
            ax.plot(x, y, label=name)
        ax.legend()
        canvas.draw()

    # Initialize data
    data = list() # List of spectra
    for path in sys.argv[1:]:
        data.extend(load_specta(path))
        
    # Create window
    root = tk.Tk()
    root.wm_title("Spectrum plot")

    # Menus
    menubar = tk.Menu(root)
    filemenu = tk.Menu(menubar, tearoff=0)
    def add_new_spectrum():
        path = tkfiledialog.askopenfilename()
        if not path:
            return
        data.extend(load_spectrum(path))
        update_plot()
    filemenu.add_command(label="Add new spectrum", command=add_new_spectrum)
    menubar.add_cascade(label="Data", menu=filemenu)
    root.config(menu=menubar)

    # Create matplotlib figure
    fig = Figure(figsize=(6, 4), dpi=100)
    ax = fig.add_subplot(111)

    # Associate this figure with a canvas created by Tk backend
    canvas = FigureCanvasTkAgg(fig, master=root)
    canvas.draw()
    canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
    toolbar = NavigationToolbar2Tk(canvas, root)
    toolbar.update()

    canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

    do_rmcontinuum = tk.BooleanVar()
    do_smoothen = tk.BooleanVar()
    do_normalize = tk.BooleanVar()
    # Parameters
    param_frame = tk.Frame(root)
    def validate_smoothwsize(P):
        global smoothwsize
        try:
            if len(P) > 0:
                smoothwsize = int(P)
                smoothwsize += 1-(smoothwsize%2)
                if do_smoothen.get() and isinstance(ax.get_children()[0], matplotlib.lines.Line2D):
                    update_plot()
            return True
        except ValueError:
            return False
    param_validate_smoothwsize = param_frame.register(validate_smoothwsize)
    label_smoothen = tk.Label(param_frame,
                              text='Smoothen window size:')
    label_smoothen.pack(side=tk.LEFT, anchor=tk.W)
    entry_smoothen = tk.Entry(param_frame,
                              width=3,
                              validate='key',
                              validatecommand=(param_validate_smoothwsize, '%P'))
    entry_smoothen.insert(0, str(smoothwsize))
    entry_smoothen.pack(side=tk.LEFT, anchor=tk.W)
    param_frame.pack(side=tk.BOTTOM, fill=tk.X, expand=0)
    # Checkboxes
    chkboxes_frame = tk.Frame(root)
    rmcontinuum_chk = tk.Checkbutton(chkboxes_frame,
                                     text='Remove continuum',
                                     variable=do_rmcontinuum,
                                     command=update_plot)
    rmcontinuum_chk.pack(side=tk.LEFT, anchor=tk.W)
    smoothen_chk = tk.Checkbutton(chkboxes_frame,
                                  text='Smoothen',
                                  variable=do_smoothen,
                                  command=update_plot)
    smoothen_chk.pack(side=tk.LEFT, anchor=tk.W)
    norm_chk = tk.Checkbutton(chkboxes_frame,
                              text='Normalize',
                              variable=do_normalize,
                              command=update_plot)
    norm_chk.pack(side=tk.LEFT, anchor=tk.W)
    chkboxes_frame.pack(side=tk.BOTTOM, fill=tk.X, expand=0)

    update_plot()
    tk.mainloop()

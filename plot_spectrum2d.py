#!/usr/bin/env python3

# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import asyncio
import csv
import getopt
import os
import sys

import numpy as np

from scipy.ndimage import gaussian_filter

from tkinter import *
from tkinter import font
from tkinter.ttk import Combobox, Separator, Treeview
import tkinter.messagebox as tkmessagebox
import tkinter.filedialog as tkfiledialog

import matplotlib
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
plt.style.use('dark_background')
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure

from spat import crismutils
from spat.cube import compose_rgb, getwvl
from spat.spectrum import rm_continuum, filter, resample, normalize
from spat.samples import db as samplesdb

# Options
measureopt = None
rmcontopt = False
smoothopt = False
smoothsigma = 2.5
normopt = False
saveopt = None
viewopt = 'rgb'

# Other state variables
rwvl = 2.5295
gwvl = 1.5066
bwvl = 1.0800
cmmin = 0.0
cmmax = 1.0

if __name__ == "__main__":
    # Callback function when stuf is changed in the GUI
    def update_ax1():
        global cube, wvl, measurements
        global canvas, ax1
        global view_mode
        global tdview_rgb_entry_r, tdview_rgb_entry_g, tdview_rgb_entry_b, tdview_cmap_entry_min, tdview_cmap_entry_max
        global rwvl, gwvl, bwvl, cmmin, cmmax
        global tdview_cmap
        # Update global parameters (we can fail early if needed)
        if view_mode.get() == 'rgb':
            try:
                rwvl = float(tdview_rgb_entry_r.get())
                gwvl = float(tdview_rgb_entry_g.get())
                bwvl = float(tdview_rgb_entry_b.get())
            except ValueError:
                tkmessagebox.showerror("Error", "Invalid value in R,G,B parameters")
                return
            except NameError:
                print('NameError')
                pass
        elif view_mode.get() == 'colormap':
            try:
                cmmin =  float(tdview_cmap_entry_min.get())
                cmmax =  float(tdview_cmap_entry_max.get())
            except ValueError:
                tkmessagebox.showerror("Error", "Invalid value in min/max parameters")
                return
            except NameError:
                print('NameError')
                pass
        # Save views limits and clear them
        ax1_xlim, ax1_ylim = ax1.get_xlim(), ax1.get_ylim()
        if len(ax1.images) > 0 and ax1.images[-1].colorbar:
            ax1.images[-1].colorbar.remove()
        ax1.cla()
        # Redraw 2D view
        if view_mode.get() == 'rgb':
            rgb = compose_rgb(cube, wvl, rwvl, gwvl, bwvl)
            img = ax1.imshow(rgb)
            # Keep space for colormap colorbar
            c = fig.colorbar(img, ax=ax1, orientation='horizontal')
            img.colorbar.ax.cla()
            img.colorbar.ax.axis('off')
        elif view_mode.get() == 'colormap':
            wvldata = getwvl(cube, wvl, (wvl[-1]-wvl[0])/2)
            img = ax1.imshow(wvldata, cmap=tdview_cmap.get(), vmin=cmmin, vmax=cmmax)
            fig.colorbar(img, ax=ax1, orientation='horizontal')
        for y, x in measurements: # maybe avoid for loop?
            ax1.plot(x, y, 'o')
        ax1.set_xlim(ax1_xlim[0], ax1_xlim[1])
        ax1.set_ylim(ax1_ylim[0], ax1_ylim[1])
        # All done, redraw canvas
        fig.canvas.draw()
    def update_ax2():
        global cube, wvl, measurements
        global canvas, ax2
        global do_rmcontinuum, do_smoothen, do_normalize
        global spview_smoothen_entry
        # Update global parameters (we can fail early if needed)
        try:
            smoothsigma = float(spview_smoothen_entry.get())
        except ValueError:
            tkmessagebox.showerror("Error", "Invalid smoothing value")
            return
        # Save views limits and clear them
        ax2_xlim, ax2_ylim = ax2.get_xlim(), ax2.get_ylim()
        ax2.cla()
        # Redraw 1D view
        specta = [cube[int(y), int(x), :] for (y, x) in measurements]
        labels = ['%d,%d'%(x,y) for (y, x) in measurements]
        for i, s in enumerate(specta): # apply various specta correction/filters
            w = np.array(wvl)
            if do_rmcontinuum.get():
                w, specta[i] = rm_continuum(w, specta[i])
            if do_smoothen.get():
                specta[i] = filter(w, specta[i], smoothsigma)
                w, specta[i] = resample(w, specta[i])
            if do_normalize.get():
                specta[i] = normalize(w, specta[i])
        ax2.grid(axis='x', color='0.5', linestyle='--')
        ax2.yaxis.tick_right()
        ax2.set_xlim(ax2_xlim[0], ax2_xlim[1])
        if do_normalize.get():
            ax2_ylim = (0.0, 1.0)
        elif len(ax2.get_lines()) == len(measurements):
            ax2.set_ylim(ax2_ylim[0], ax2_ylim[1])
        else:
            ax2.autoscale(axis='y')
        for s, l in zip(specta, labels):
            ax2.plot(w, s, label=l, zorder=2)
        for k in samplesvars:
            if samplesvars[k].get():
                ax2.plot(samplesdb[k][0], samplesdb[k][1], label=k, zorder=1)
        if len(labels)>0:
            ax2.legend()
        # All done, redraw canvas
        fig.canvas.draw()
    def show_metadata():
        global wvl
        mdwin = Toplevel()
        mdwin.title('Metadata')
        mdwin.geometry('320x600')
        frame = Frame(mdwin)
        tv = Treeview(frame)
        tv['columns']=('Band', 'Wavelength')
        tv.column('#0', width=0, stretch=NO)
        tv.column('Band', anchor=CENTER, width=100)
        tv.column('Wavelength', anchor=CENTER, width=200)
        tv.heading('#0', text='', anchor=CENTER)
        tv.heading('Band', text='Band', anchor=CENTER)
        tv.heading('Wavelength', text='Wavelength', anchor=CENTER)
        for i,w in enumerate(wvl):
            tv.insert(parent='', index=i, iid=i, text='', values=('%d'%(i+1),str(w)))
        tv.pack(side=LEFT, fill=BOTH, expand=TRUE)
        scrollbar = Scrollbar(frame, command=tv.yview)
        tv.configure(yscrollcommand=scrollbar.set)
        scrollbar.pack(side=RIGHT, fill=Y)
        frame.pack(side=LEFT, fill=BOTH, expand=TRUE)
    def clear_measurements():
        global measurements
        measurements = []
        update_ax1()
        update_ax2()
    def load_cube(path=None, erase_measurements=True):
        global root, fig, cube, wvl, measurements
        if not path:
            path = tkfiledialog.askopenfilename()
        if not path:
            return
        root.wm_title(path)
        fig.suptitle(os.path.basename(path))
        cube, wvl = crismutils.loadcube(path)
        if erase_measurements:
            measurements = []
        ax1.set_ylim(cube.shape[0]-0.5, -0.5)
        ax1.set_xlim(-0.5, cube.shape[1]-0.5)
        ax2.set_xlim(wvl[0], wvl[-1])
        update_ax1()
        update_ax2()
    def do_export_csv():
        global ax2
        # TODO: check if there's a plot
        f = tkfiledialog.asksaveasfile(mode='w', defaultextension=".csv")
        if f:
            writer = csv.writer(f)
            xs, ys = ax2.get_children()[0].get_data()
            for x, y in zip(xs, ys):
                writer.writerow((x, y))
            f.close()

    opts, argv = getopt.getopt(sys.argv[1:], 'm:f:csno:v:')
    for o, a in opts:
        if o == '-m': measureopt = list(zip(*[iter(map(float, a.split(',')))] * 2))
        elif o == '-c': rmcontopt = True
        elif o == '-s': smoothopt = True
        elif o == '-n': normopt = True
        elif o == '-o': saveopt = a
        elif o == '-v':
            if a in ['rgb', 'colormap']: viewopt = a
            else: assert False, 'Wrong value "%s" for option -v'%(a)
        else:
            assert False, 'Unknown option'
    cube = None
    measurements = measureopt if measureopt else [] 

    # Create window
    root = Tk()
    root.wm_title('plot_spectrum2d')
    root.geometry('800x600')
    defaultfont = font.Font(family='Helvetica', size=10)
    root.option_add('*Font', defaultfont)

    # Interface variables
    view_mode = StringVar()
    view_mode.set(viewopt)
    tdview_cmap = StringVar()
    tdview_cmap.set('viridis')
    do_rmcontinuum = BooleanVar()
    do_rmcontinuum.set(rmcontopt)
    do_smoothen = BooleanVar()
    do_smoothen.set(smoothopt)
    do_normalize = BooleanVar()
    do_normalize.set(normopt)

    # Menus
    menubar = Menu(root)
    datamenu = Menu(menubar, tearoff=0)
    datamenu.add_command(label='Open cube', command=load_cube)
    datamenu.add_command(label='Show metadata', command=show_metadata)
    datamenu.add_command(label='Clear measurements', command=clear_measurements)
    datamenu.add_command(label='Export measurements', command=do_export_csv)
    menubar.add_cascade(label='Data', menu=datamenu)
    spectralviewmenu = Menu(menubar, tearoff=0)
    samplesmenu = Menu(spectralviewmenu, tearoff=0)
    samplesvars = {}
    for k in samplesdb:
        samplesvars[k] = BooleanVar()
        samplesmenu.add_checkbutton(label=k,
                                    onvalue=1, offvalue=0,
                                    variable=samplesvars[k],
                                    command=update_ax2)
    spectralviewmenu.add_cascade(label="Reference spectra", menu=samplesmenu)
    menubar.add_cascade(label="Spectral view", menu=spectralviewmenu)
    root.config(menu=menubar)

    # Create plots
    fig = Figure(figsize=(6, 4), dpi=100)
    fig.subplots_adjust(left=0.05, right=0.95, wspace=0.04)
    ax1, ax2 = fig.subplots(1, 2, gridspec_kw={'width_ratios': [1, 1.5]})
    ax1.margins(x=0)
    ax2.margins(x=0)

    # Associate this figure with a canvas created by Tk backend
    canvas = FigureCanvasTkAgg(fig, master=root)
    canvas._tkcanvas.pack(side=TOP, fill=BOTH, expand=1)
    canvas.draw()
    canvas.get_tk_widget().pack(side=TOP, fill=BOTH, expand=1)
    toolbar = NavigationToolbar2Tk(canvas, root)
    toolbar.update()

    bottom_frame = Frame(root)
    bottom_frame.pack(side=TOP, fill=X, expand=0)
    # 2D view settings
    tdview_params_frame = Frame(bottom_frame)
    tdview_params_frame.grid(row=0, column=0, padx=5, sticky='ew')
    tdview_params_frame_1 = Frame(tdview_params_frame)
    tdview_params_frame_1.pack(side=TOP, fill=X, expand=0)
    tdview_mode_label = Label(tdview_params_frame_1,
                              text='2D view mode:')
    tdview_mode_label.pack(side=LEFT, anchor=N)
    tdview_mode_combobox = Combobox(tdview_params_frame_1,
                                    textvariable=view_mode,
                                    state='readonly',
                                    values=('rgb', 'colormap'))
    tdview_mode_combobox.pack(side=LEFT, anchor=N)
    tdview_params_frame_2 = Frame(tdview_params_frame)
    tdview_params_frame_2.pack(side=TOP, fill=X, expand=0)
    tdview_params_frame_3 = Frame(tdview_params_frame)
    tdview_params_frame_3.pack(side=TOP, fill=X, expand=0)
    def do_tdview_params_frames(event):
        global tdview_params_frame_2, tdview_params_frame_3
        tdview_params_frame_2.destroy()
        tdview_params_frame_2 = Frame(tdview_params_frame)
        tdview_params_frame_2.pack(side=TOP, fill=X, expand=0)
        tdview_params_frame_3.destroy()
        tdview_params_frame_3 = Frame(tdview_params_frame)
        tdview_params_frame_3.pack(side=TOP, fill=X, expand=0)
        if view_mode.get() == 'rgb':
            global tdview_rgb_entry_r, tdview_rgb_entry_g, tdview_rgb_entry_b
            tdview_rgb_label = Label(tdview_params_frame_2,
                                     text='RGB wavelengths:')
            tdview_rgb_label.pack(side=LEFT, anchor=W)
            tdview_rgb_entry_r = Entry(tdview_params_frame_3, width=7)
            tdview_rgb_entry_r.pack(side=LEFT, anchor=W)
            tdview_rgb_entry_r.insert(0, str(rwvl))
            tdview_rgb_entry_r.bind('<Return>', (lambda event: update_ax1()))
            tdview_rgb_entry_g = Entry(tdview_params_frame_3, width=7)
            tdview_rgb_entry_g.pack(side=LEFT, anchor=W)
            tdview_rgb_entry_g.insert(0, str(gwvl))
            tdview_rgb_entry_g.bind('<Return>', (lambda event: update_ax1()))
            tdview_rgb_entry_b = Entry(tdview_params_frame_3, width=7)
            tdview_rgb_entry_b.pack(side=LEFT, anchor=W)
            tdview_rgb_entry_b.insert(0, str(bwvl))
            tdview_rgb_entry_b.bind('<Return>', (lambda event: update_ax1()))
        elif view_mode.get() == 'colormap':
            global tdview_cmap_entry_min, tdview_cmap_entry_max
            tdview_cmap_label = Label(tdview_params_frame_2,
                                      text='Color map:')
            tdview_cmap_label.pack(side=LEFT, anchor=W)
            tdview_cmap_combobox = Combobox(tdview_params_frame_2,
                                            textvariable=tdview_cmap,
                                            state='readonly',
                                            values=('viridis', 'magma', 'jet'))
            tdview_cmap_combobox.pack(side=LEFT)
            tdview_cmap_combobox.current(0)
            tdview_cmap_combobox.bind("<<ComboboxSelected>>", (lambda event: update_ax1()))
            tdview_cmap_minlabel = Label(tdview_params_frame_3, text='Min:')
            tdview_cmap_minlabel.pack(side=LEFT, anchor=W)
            tdview_cmap_entry_min = Entry(tdview_params_frame_3, width=9)
            tdview_cmap_entry_min.pack(side=LEFT, anchor=W)
            tdview_cmap_entry_min.insert(0, str(cmmin))
            tdview_cmap_entry_min.bind('<Return>', (lambda event: update_ax1()))
            tdview_cmap_maxlabel = Label(tdview_params_frame_3, text='Max:')
            tdview_cmap_maxlabel.pack(side=LEFT, anchor=W)
            tdview_cmap_entry_max = Entry(tdview_params_frame_3, width=9)
            tdview_cmap_entry_max.pack(side=LEFT, anchor=W)
            tdview_cmap_entry_max.insert(0, str(cmmax))
            tdview_cmap_entry_max.bind('<Return>', (lambda event: update_ax1()))
        if cube is not None:
            update_ax1()
    tdview_mode_combobox.bind("<<ComboboxSelected>>", do_tdview_params_frames)
    do_tdview_params_frames(None)
    # Separator between 2d view/spectral view parameters
    tdspsep = Separator(bottom_frame, orient=VERTICAL)
    tdspsep.grid(row=0, column=1, padx=5, sticky='ns')
    # Spectral view settings
    spview_params_frame = Frame(bottom_frame)
    spview_params_frame.grid(row=0, column=2, padx=5, sticky='ew')
    spview_params_frame_1 = Frame(spview_params_frame)
    spview_params_frame_1.pack(side=TOP, fill=X, expand=0)
    spview_rmcontinuum_chk = Checkbutton(spview_params_frame_1,
                                         text='Remove continuum',
                                         variable=do_rmcontinuum,
                                         command=update_ax2)
    spview_rmcontinuum_chk.pack(side=LEFT, anchor=W)
    spview_smoothen_chk = Checkbutton(spview_params_frame_1,
                                      text='Smoothen',
                                      variable=do_smoothen,
                                      command=update_ax2)
    spview_smoothen_chk.pack(side=LEFT, anchor=W)
    spview_norm_chk = Checkbutton(spview_params_frame_1,
                                  text='Normalize',
                                  variable=do_normalize,
                                  command=update_ax2)
    spview_norm_chk.pack(side=LEFT, anchor=W)
    spview_params_frame_2 = Frame(spview_params_frame)
    spview_params_frame_2.pack(side=TOP, fill=X, expand=0)
    spview_smoothen_label = Label(spview_params_frame_2,
                                  text='Smoothen sigma:')
    spview_smoothen_label.pack(side=LEFT, anchor=W)
    spview_smoothen_entry = Entry(spview_params_frame_2,
                                  width=3)
    spview_smoothen_entry.pack(side=LEFT, anchor=W)
    spview_smoothen_entry.insert(0, str(smoothsigma))
    spview_smoothen_entry.bind('<Return>', (lambda event: update_ax2() if do_smoothen.get() else None))

    # Add handlers to manage ctrl+clic events
    def onbuttonpress(event):
        global toolbar, ax1, measurements
        if toolbar.mode != '':
            return
        if event.inaxes != ax1:
            return
        measurements.append((event.ydata, event.xdata))
        update_ax1()
        update_ax2()
    fig.canvas.mpl_connect('button_press_event', onbuttonpress)

    if saveopt:
        if len(argv) == 1:
            load_cube(path=argv[0], erase_measurements=False)
        fig.savefig(saveopt, dpi=150)
    else:
        if len(argv) == 1:
            async def load_cube_async():
                load_cube(path=argv[0], erase_measurements=False)
            asyncio.run(load_cube_async())
        mainloop()

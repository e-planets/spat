# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import os
import numpy as np
from osgeo import gdal

gdal.DontUseExceptions()

# Load a CRISM CAT cube, return data and wavelengths
def loadcube(path):
    ds = gdal.Open(path)
    if not ds:
        exit(1)
    xsize, ysize, zsize = ds.RasterXSize, ds.RasterYSize, ds.RasterCount
    md = ds.GetMetadata('ENVI')
    cube = np.empty((ysize, xsize, zsize))
    wvls = []
    wvlmult = 1.
    if 'wavelength_units' in md:
        if md['wavelength_units'].lower() in ['nanometers', 'nm']:
            wvlmult = 1./1000.
        elif md['wavelength_units'].lower() in ['micrometers', 'um']:
            wvlmult = 1.
        else:
            print('Unhandled unit', md['wavelength_units'])
    for i in range(zsize):
        b = ds.GetRasterBand(i+1)
        bmd = b.GetMetadata()
        wvls.append((float(bmd['wavelength']) * wvlmult) if 'wavelength' in bmd else i)
        b.ReadAsArray(buf_obj=cube[:,:,i])
    cube[cube == ds.GetRasterBand(1).GetNoDataValue()] = np.nan
    return (cube, wvls)

# Load the geographic lookup table associated with a CRISM CAT cube
def loadglt(inpath):
    # The corresponding DDR image has the emission angles and coordinates
    inbasename = os.path.basename(inpath)
    de = "_DE" if inbasename[15:17].isupper() else "_de"
    ddr1 = "_DDR1" if inbasename[22:26].isupper() else "_ddr1"
    ddrpath = os.path.join(os.path.dirname(inpath),
                           inbasename[0:14]+de+inbasename[17:21]+ddr1+".img")
    ds = gdal.Open(ddrpath)
    if not ds:
        raise Exception('Cannot open DDR1 file corresponding to input')
    xsize, ysize, zsize = ds.RasterXSize, ds.RasterYSize, ds.RasterCount
    ema = ds.GetRasterBand(2).ReadAsArray()
    latlt = ds.GetRasterBand(4).ReadAsArray()
    lonlt = ds.GetRasterBand(5).ReadAsArray()
    return latlt, lonlt

# Convert a geographic lookup table to a pixel lookup table
def glt2plt(lonlt, latlt, xsize, ysize):
    lonmin, lonmax = lonlt.min(), lonlt.max()
    latmin, latmax = latlt.min(), latlt.max()
    xlt = (lonlt - lonmin) / (lonmax - lonmin) * (xsize-1)
    ylt = (ysize-1) - ((latlt - latmin) / (latmax - latmin) * (ysize-1))
    return xlt, ylt

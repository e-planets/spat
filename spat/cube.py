# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as np

# Generic function to get the nearest layer for a given wavelength in cube
def findwvlidx(cube, wvls, target):
    best, best_score = 0, abs(target - wvls[0])
    for i in range(1, len(wvls)):
        score = abs(target - wvls[i])
        if score < best_score:
            best, best_score = i, score
    return best

# This is the function that will return an averaged layer for a given
# target wavelength
def getwvl(cube, wvls, target):
    idx = findwvlidx(cube, wvls, target)
    return np.average(cube[:,:,idx-2:idx+2], 2)

# Do a RGB composition from 3 wavelengths
def compose_rgb(cube, wvls, r, g, b):
    rgb = np.empty((cube.shape[0], cube.shape[1], 3), 'uint8')
    for i, c in enumerate([r, g, b]):
        x = getwvl(cube, wvls, c)
        if np.isnan(x).all():
            rgb[:, :, i] = 0
        else:
            xmin, xmax = np.nanmin(x), np.nanmax(x)
            if xmin != xmax:
                rgb[:, :, i] = np.nan_to_num((x-xmin)/(xmax-xmin)*255.0, 0, 255).astype('uint8')
            else:
                rgb[:, :, i] = 0
    return rgb

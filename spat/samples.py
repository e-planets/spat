# Copyright CNRS

#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as np

# The samples (private) database structure
#
# This is a dict, the key is the sample name, and hold a 2-values
# tuple with wavelengths (in nm) and values (normalized). Use numpy
# arrays.
db = {

'sample_1' : (
# Wavelengths (nm)
np.array([1000, 1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450, 1500,
          1550, 1600, 1650, 1700, 1750, 1800, 1850, 1900, 1950, 2000, 2050,
          2100, 2150, 2200, 2250, 2300, 2350, 2400, 2450, 2500, 2550, 2600,
          2650, 2700, 2750, 2800, 2850, 2900, 2950, 3000, 3050, 3100, 3150,
          3200, 3250, 3300, 3350, 3400, 3450, 3500, 3550, 3600, 3650, 3700,
          3750, 3800, 3850, 3900, 3950]),
# Values (normalized)
np.array([1.38879439e-11, 1.58939101e-10, 1.60522806e-09, 1.43072419e-08,
          1.12535175e-07, 7.81148941e-07, 4.78511739e-06, 2.58681002e-05,
          1.23409804e-04, 5.19574682e-04, 1.93045414e-03, 6.32971543e-03,
          1.83156389e-02, 4.67706224e-02, 1.05399225e-01, 2.09611387e-01,
          3.67879441e-01, 5.69782825e-01, 7.78800783e-01, 9.39413063e-01,
          1.00000000e+00, 9.39413063e-01, 7.78800783e-01, 5.69782825e-01,
          3.67879441e-01, 2.09611387e-01, 1.05399225e-01, 4.67706224e-02,
          1.83156389e-02, 6.32971543e-03, 1.93045414e-03, 5.19574682e-04,
          1.23409804e-04, 2.58681002e-05, 4.78511739e-06, 7.81148941e-07,
          1.12535175e-07, 1.43072419e-08, 1.60522806e-09, 1.58939101e-10,
          1.38879439e-11, 1.07092324e-12, 7.28772410e-14, 4.37661850e-15,
          2.31952283e-16, 1.08485526e-17, 4.47773244e-19, 1.63101392e-20,
          5.24288566e-22, 1.48729218e-23, 3.72336312e-25, 8.22598060e-27,
          1.60381089e-28, 2.75950907e-30, 4.19009319e-32, 5.61472809e-34,
          6.63967720e-36, 6.92912494e-38, 6.38150345e-40, 5.18657681e-42])
),

}

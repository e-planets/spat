# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as np

from scipy.interpolate import interp1d
from scipy.spatial import ConvexHull
try:
    from scipy.ndimage import gaussian_filter1d
except ImportError:
    # Old scipy gaussian filter
    from scipy.signal import gaussian
    def gaussian_filter1d(input, sigma):
        size = int(np.ceil(sigma*3.0 / 2) * 2 + 1)
        gauss = scipy.signal.gaussian(size, sigma)
        filtered = np.convolve(gauss, val, "valid")/sum(gauss)
        return np.concatenate(input[0:int(size/2)], filtered, input[-int(size/2):])

# Remove continuum using a hull evaluation
def rm_continuum(wvl, val):
    wvl2, val2 = [], []
    for w, v in zip(wvl, val):
        if np.isfinite(v):
            wvl2.append(w)
            val2.append(v)
    wvl2, val2 = np.array(wvl2), np.array(val2)
    points = np.vstack((wvl2, val2)).T
    hull = ConvexHull(points)
    f = interp1d(wvl2[hull.vertices],
                 val2[hull.vertices],
                 kind="quadratic")
    newval = val2 - f(wvl2)
    return wvl2, newval

# Apply a gaussian filtering
def filter(wvl, val, sigma=2.5):
    return gaussian_filter1d(val, sigma)

# Resample the data to allow smoother zooming
def resample(wvl, val):
    wvl2 = np.ma.compressed(np.ma.masked_where(~(np.isfinite(val)), wvl))
    val2 = np.ma.compressed(np.ma.masked_invalid(val))
    newwvl = np.linspace(wvl2[0], wvl2[-1], wvl2.shape[0]*5)
    f = interp1d(wvl2, val2, kind='cubic')
    newval = f(newwvl)
    return newwvl, newval

# Normalize data
def normalize(wvl, val):
    val2 = np.ma.compressed(np.ma.masked_invalid(val))
    n = np.linalg.norm(val2, np.inf)
    return val if n == 0 else (val / n)

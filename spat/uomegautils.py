# Copyright CNRS
#  contributors: Matthieu Volat <matthieu.volat@univ-lyon1.fr>
#
# Spectrum Plotting and Analysis Toolkit
#
# This software is governed by the CeCILL  license under French law and
# abiding by the rules of distribution of free software.  You can  use,
# modify and/ or redistribute the software under the terms of the CeCILL
# license as circulated by CEA, CNRS and INRIA at the following URL
# "http://www.cecill.info".
#
# As a counterpart to the access to the source code and  rights to copy,
# modify and redistribute granted by the license, users are provided only
# with a limited warranty  and the software's author,  the holder of the
# economic rights,  and the successive licensors  have only  limited
# liability.
#
# In this respect, the user's attention is drawn to the risks associated
# with loading,  using,  modifying and/or developing or reproducing the
# software by the user in light of its specific status of free software,
# that may mean  that it is complicated to manipulate,  and  that  also
# therefore means  that it is reserved for developers  and  experienced
# professionals having in-depth computer knowledge. Users are therefore
# encouraged to load and test the software's suitability as regards their
# requirements in conditions enabling the security of their systems and/or
# data to be ensured and,  more generally, to use and operate it in the
# same conditions as regards security.
#
# The fact that you are presently reading this means that you have had
# knowledge of the CeCILL license and that you accept its terms.

import numpy as np

uomega_wvls = [
    0.595  , 0.643  , 0.77   , 0.885  , 1.00724, 1.01117, 1.01729,
    1.02352, 1.02978, 1.03615, 1.04257, 1.04901, 1.0556 , 1.06216,
    1.06898, 1.07578, 1.08268, 1.08969, 1.09684, 1.10395, 1.11136,
    1.11874, 1.12618, 1.13341, 1.14158, 1.14929, 1.15725, 1.16525,
    1.17342, 1.18173, 1.19003, 1.19856, 1.20718, 1.2159 , 1.22487,
    1.23384, 1.24299, 1.25219, 1.26176, 1.27131, 1.28106, 1.29091,
    1.30098, 1.31105, 1.32152, 1.33208, 1.34276, 1.34693, 1.34998,
    1.35373, 1.3574 , 1.36102, 1.36475, 1.36858, 1.37226, 1.37595,
    1.37967, 1.38355, 1.38735, 1.39096, 1.39511, 1.39904, 1.40286,
    1.40687, 1.41075, 1.41482, 1.41883, 1.42294, 1.42691, 1.43101,
    1.43503, 1.43807, 1.44336, 1.44757, 1.45181, 1.45603, 1.46044,
    1.46457, 1.46874, 1.47315, 1.47753, 1.48197, 1.48633, 1.4907 ,
    1.49518, 1.49965, 1.50417, 1.50874, 1.51329, 1.51791, 1.52251,
    1.52701, 1.53197, 1.53659, 1.54116, 1.54618, 1.55086, 1.55559,
    1.5607 , 1.56585, 1.57052, 1.57536, 1.58024, 1.58538, 1.59056,
    1.5956 , 1.60059, 1.60581, 1.61098, 1.61619, 1.62141, 1.62676,
    1.63211, 1.63734, 1.64288, 1.64826, 1.65378, 1.65922, 1.66493,
    1.67034, 1.676  , 1.6816 , 1.68723, 1.6932 , 1.6986 , 1.70469,
    1.71014, 1.71645, 1.72246, 1.72821, 1.73484, 1.74025, 1.74645,
    1.75279, 1.75954, 1.765  , 1.77134, 1.77785, 1.78423, 1.79058,
    1.79707, 1.80355, 1.81107, 1.81657, 1.82337, 1.82998, 1.83662,
    1.8436 , 1.85035, 1.85751, 1.86433, 1.87139, 1.87827, 1.88559,
    1.89274, 1.89999, 1.90771, 1.9146 , 1.9221 , 1.92963, 1.9369 ,
    1.94467, 1.95221, 1.9599 , 1.96775, 1.97436, 1.98614, 1.99138,
    1.99926, 2.00752, 2.01576, 2.02385, 2.03231, 2.04052, 2.04909,
    2.0573 , 2.06608, 2.07473, 2.08342, 2.09211, 2.10108, 2.10992,
    2.11904, 2.12831, 2.13747, 2.14645, 2.15536, 2.16529, 2.17488,
    2.18442, 2.1941 , 2.20381, 2.21357, 2.22362, 2.23361, 2.24367,
    2.25393, 2.26418, 2.27454, 2.28482, 2.29577, 2.30639, 2.31734,
    2.32815, 2.33904, 2.35037, 2.36126, 2.37241, 2.38414, 2.39555,
    2.40737, 2.41899, 2.43121, 2.43914, 2.4573 , 2.46739, 2.47973,
    2.49203, 2.50485, 2.51773, 2.5307 , 2.54361, 2.55667, 2.57035,
    2.58347, 2.59719, 2.61072, 2.62451, 2.63863, 2.65276, 2.6673 ,
    2.6815 , 2.69644, 2.71089, 2.72612, 2.74097, 2.75687, 2.772  ,
    2.7876 , 2.80331, 2.81944, 2.83571, 2.85189, 2.86848, 2.88542,
    2.9024 , 2.91962, 2.93673, 2.95442, 2.97216, 2.99028, 3.00869,
    3.02779, 3.04604, 3.06448, 3.08399, 3.1035 , 3.12297, 3.14272,
    3.16269, 3.18327, 3.19934, 3.21507, 3.23096, 3.23585, 3.24603,
    3.25691, 3.26782, 3.27825, 3.29041, 3.30023, 3.31207, 3.32332,
    3.33438, 3.34681, 3.35739, 3.36909, 3.38026, 3.39319, 3.40312,
    3.41521, 3.42712, 3.43922, 3.45144, 3.46303, 3.47581, 3.48998,
    3.50031, 3.51311, 3.52507, 3.5378 ]

def loadcube(path):
    xsize, ysize, zsize = 256, 256, len(uomega_wvls)
    cube = np.empty((ysize, xsize, zsize))
    rawfile = open(path)
    for z in range(zsize):
        for y in range(ysize):
            for x in range(xsize):
                cube[y, x, z] = float(rawfile.readline())
    return (cube, uomega_wvls)
